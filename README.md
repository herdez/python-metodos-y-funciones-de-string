## Métodos y funciones de string en Python

Python tiene métodos que pueden ser aplicados para desarrollar una tarea sobre un string, asimismo tiene también funciones que pueden tomar como parámetro un string y desarrollar cierta tarea.

#### Algunos métodos y funciones de string 

```python
#count
#index
#isdecimal
#center
#capitalize
#find
#len
#lower
#upper
#strip
#split
#startswith
#max
#min
```

## Ejercicio - Ejemplo de Métodos de String

De la lista de métodos y funciones de string proporciona un ejemplo del uso de cada uno.

## Ejercicio - Aplicando métodos y funciones de string

Aplica el método de string que corresponda en el espacio indicado `[aplica método de string]`, de tal manera que el resultado de la comparación sea `true`

```
hola = "bienvenido"
hola2 = "al curso"
string_result = #¿Qué se asignaría a esta variable para que la siguiente expresión sea True
string_result.[aplica método de string] == 'BIENVENIDO    AL CURSO   '
#True
string_result.[aplica método de string].[aplica método de string] == 'Bienvenido    al curso'
#True
[aplica método de string o funcion a](string_result) == 25
#True
string_result.[aplica método de string] == 2
#True
string_result.[aplica método de string].[aplica método de string] == ['bienvenido', 'este', 'sabado']
#True
variable = #¿Qué se asignaría a esta variable para que la última expresión sea True
[aplica método de string o funcion a esta](variable) == 3
#True
```



> Esta documentación muestra los métodos y funciones de string de python: [Python String Methods](https://www.programiz.com/python-programming/methods/string).